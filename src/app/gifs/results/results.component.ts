import { Gif } from './../interfaces/gifs.interface';
import { GifsService } from './../services/gifs.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styles: [],
})
export class ResultsComponent {
  get results(): Gif[] {
    return this.gifsService.results;
  }

  constructor(private gifsService: GifsService) {}
}
