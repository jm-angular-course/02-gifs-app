import { GifsService } from './../services/gifs.service';
import { Component, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [],
})
export class SearchComponent {
  @ViewChild('txtSearch')
  txtSearch!: ElementRef<HTMLInputElement>;

  constructor(private gifsService: GifsService) {}

  search(): void {
    const value = this.txtSearch.nativeElement.value;
    if (!value.trim().length) return;
    this.gifsService.searchGifs(value);
    this.txtSearch.nativeElement.value = '';
  }
}
