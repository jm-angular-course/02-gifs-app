import { Gif, SearchGifsResponse } from './../interfaces/gifs.interface';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GifsService {
  private readonly historyKey: string = 'history';
  private readonly resultsKey: string = 'results';
  private readonly baseUri: string = 'https://api.giphy.com/v1/gifs/';
  private readonly apiKey: string = 'zFEtnfhsg7DKBXrei7CgjZpu7MSfMznI';
  private _history: string[] = [];
  public results: Gif[] = [];

  // romper referencia con la propiedad del servicio
  // no se puede modificar desde afuera
  get history(): string[] {
    return [...this._history];
  }

  constructor(private http: HttpClient) {
    this._history = JSON.parse(localStorage.getItem(this.historyKey)!) || [];
    this.results = JSON.parse(localStorage.getItem(this.resultsKey)!) || [];
  }

  searchGifs(query: string): void {
    query = query.trim().toLowerCase();
    this.addQueryToHistory(query);

    const params = new HttpParams()
      .set('api_key', this.apiKey)
      .set('q', query)
      .set('limit', '10');
    this.http
      .get<SearchGifsResponse>(`${this.baseUri}search`, { params })
      .subscribe((res) => {
        this.results = res.data;
        localStorage.setItem(this.resultsKey, JSON.stringify(this.results));
      });
  }

  private addQueryToHistory(query: string) {
    if (this._history.includes(query)) {
      this._history = this._history.filter(
        (element: string) => element !== query
      );
    }
    this._history.unshift(query);
    this._history = this._history.splice(0, 10);
    localStorage.setItem(this.historyKey, JSON.stringify(this._history));
  }
}
