import { GifsService } from './../../gifs/services/gifs.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent {
  get history(): string[] {
    return this.gifsService.history;
  }

  constructor(private gifsService: GifsService) {}

  search(query: string) {
    this.gifsService.searchGifs(query);
  }
}
